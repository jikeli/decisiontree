package DataModel;

/*
 * TEAM 16 
 * This is the data class to record a row of data
 * 
 */

import java.util.HashMap;

public class Data {
	String label;
	
	//Map to numeric attributes
	HashMap<String, Double> numAttrMap;
	
	//Map to discrete attributes
	HashMap<String, String> discAttrMap;
	
	public Data() {
		numAttrMap = new HashMap<String, Double>();
		discAttrMap = new HashMap<String, String>();
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	public void setNumericAttr(String attrName, double attr) {
		numAttrMap.put(attrName, attr);
	}
	
	public void setDiscreteAttr(String attrName, String attr) {
		this.discAttrMap.put(attrName, attr);
	}
	
	public double getNumericAttr(String attr) {
		return numAttrMap.get(attr);
	}
	
	public String getDiscreteAttr(String attr) {
		return discAttrMap.get(attr);
	}
	
	public String getLabel() {
		return label;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (String key : numAttrMap.keySet()) sb.append(numAttrMap.get(key) + " "); 
		for (String key : discAttrMap.keySet()) sb.append(discAttrMap.get(key) + " "); 
		return sb.toString();
	}
}
