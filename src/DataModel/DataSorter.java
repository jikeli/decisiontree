package DataModel;

/*
 * TEAM 16 
 * Implementation of sorting the data according to a
 * attribute, based on quick sort.
 * 
 */

import java.util.ArrayList;
import java.util.Collections;

public class DataSorter {
	
	public static void dataSort(ArrayList<Data> dataSet, String attr) {
		quickSort(dataSet, 0, dataSet.size() - 1, attr);
	}
	
	public static void quickSort(ArrayList<Data> dataSet, int l, int r, String attr) {
		if (l == r) return;
		int index = partition(dataSet, l , r, attr);
		if (l < index - 1) quickSort(dataSet, l, index - 1, attr);
		if (index < r) quickSort(dataSet, index, r, attr);
	}

	private static int partition(ArrayList<Data> dataSet, int l, int r, String attr) {
		double mid = dataSet.get(l + (r - l) / 2).getNumericAttr(attr);
		while (l <= r) {
			while (dataSet.get(l).getNumericAttr(attr) < mid) l++;
			while (dataSet.get(r).getNumericAttr(attr) > mid) r--;
			if (l <= r) {
				Collections.swap(dataSet, l, r);
				l++;
				r--;
			}
		}
		return l;
	} 

}