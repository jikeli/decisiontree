package DataModel;

/*
 * TEAM 16 
 * Implementation of reading and formatting the input data
 * 
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class DataScanner {
	// Get all the possible labels
	public String[] getLabels(String fileName) {
		Scanner scanner = null;
		String[] labels;

		try {
			scanner = new Scanner(new File(fileName));
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				// @attribute label {C1,C2,C3,C4,C5}
				if (line.startsWith("@attribute label") ||
						line.startsWith("@attribute Label") 	) {
					int l = line.indexOf("{");
					int r = line.indexOf("}");
					String subline = line.substring(l + 1, r);
					labels = subline.split(",");
					return labels;
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("Cannot find the file");
		} finally {
			if (scanner != null) scanner.close();
		}
		return null;

	}

	// Get all the attributes
	public HashMap<String, String[]> getAttributes(String fileName) {
		Scanner scanner = null;
		HashMap<String, String[]> attrs = new HashMap<String, String[]>();

		try {
			scanner = new Scanner(new File(fileName));
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (line.startsWith("@attribute")) {
					if (line.contains("Label") ||
							line.contains("label")) break;

					// This is a real type attribute
					if (line.endsWith("real")) {
						int l = line.indexOf("e") + 1;
						int r = line.lastIndexOf("r");
						String currAttr = line.substring(l, r).trim();
						attrs.put(currAttr, null);
					} else { // This is a discrete type attribute
						int first = line.indexOf("e") + 1;
						int l = line.indexOf("{");
						int r = line.indexOf("}");
						String currAttr = line.substring(first, l).trim();
						String subline = line.substring(l + 1, r).trim();
						String[] attrTypes = subline.split(",");
						attrs.put(currAttr, attrTypes);
					}
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("Cannot find the file");
		} finally {
			if (scanner != null) scanner.close();
		}
		return attrs;
	}
	
	
	//Return all the attributes name as an array
	public String[] getAttributesArray(String fileName) {
		Scanner scanner = null;
		ArrayList<String> attrs = new ArrayList<String>();

		try {
			scanner = new Scanner(new File(fileName));
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (line.startsWith("@attribute")) {
					if (line.contains("label") ||
							line.contains("Label") ) break;

					String currAttr = null;
					// This is a real type attribute
					if (line.endsWith("real")) {
						int l = line.indexOf("e") + 1;
						int r = line.lastIndexOf("r");
						currAttr = line.substring(l, r).trim();
					} else { // This is a discrete type attribute
						int l = line.indexOf("e") + 1;
						int r = line.indexOf("{");
						currAttr = line.substring(l, r).trim();
					}
					attrs.add(currAttr);
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("Cannot find the file");
		} finally {
			if (scanner != null) scanner.close();
		}
		return (String[]) attrs.toArray(new String[1]);
	}

	public ArrayList<Data> scan(String fileName) {
		Scanner scanner = null;
		HashMap<String, String[]> attrs = getAttributes(fileName);
		String[] attrNames = getAttributesArray(fileName);
		
		ArrayList<Data> dataSet = new ArrayList<Data>();
		try {
			scanner = new Scanner(new File(fileName));
			boolean isData = false;

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();

				// Find the data columns
				if (line.startsWith("@data")) {
					isData = true;
					continue;
				}
				if (!isData || line.length() == 0) continue;
				Data curr = new Data();
				String[] currAttrs = line.split(",");
				for (int i = 0; i < attrNames.length; i++) {
					// Real type
					if (attrs.get(attrNames[i]) == null) {
						curr.setNumericAttr(attrNames[i], Double.parseDouble(currAttrs[i]));
					} else { // Discrete type
						curr.setDiscreteAttr(attrNames[i], currAttrs[i]);
					}
				}
				curr.setLabel(currAttrs[currAttrs.length - 1]);
				dataSet.add(curr);
			}
		} catch (FileNotFoundException e) {
			System.err.println("Cannot find the file");
		} finally {
			if (scanner != null) scanner.close();
		}
		return dataSet;
	}

}
