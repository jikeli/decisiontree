package DecisionTree;
/*
 * TEAM 16 
 * The class of nodes of Decision Tree
 * 
 */
import java.util.HashMap;

public class TreeNode {
	// If label is not null, this terminal leaf
	String label;
	String attribute;
	boolean isNumeric;
	double splitVal;
	
	//For discrete attributes
	HashMap<String, TreeNode> splitAttrMap;

	// For numeric attributes
	TreeNode left;
	TreeNode right;

	public TreeNode(String attr, boolean isNumeric) {
		this.attribute = attr;
		this.isNumeric = isNumeric;
		label = null;
		left = null;
		right = null;
		if (isNumeric)
			splitAttrMap = null;
		else
			splitAttrMap = new HashMap<String, TreeNode>();
	}

	//For leaf nodes
	public TreeNode(String label) {
		this.label = label;
		label = null;
		splitAttrMap = null;
		left = null;
		right = null;
	}
}
