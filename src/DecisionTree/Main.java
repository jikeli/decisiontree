package DecisionTree;
/*
 * Team 16
 * Set the input data here.
 */


public class Main {
	private static final String trainDataA = "trainProdSelection.arff";
	private static final String testDataA = "testProdSelection.arff";
	private static final String trainDataB = "trainProdIntro.binary.arff";
	private static final String testDataB = "testProdIntro.binary.arff";
	
	public static void main(String[] args) {
		/*
		 * Input the filename of training data and test data, the 
		 * arff file should be located in the root file of the project
		 */	
		DecisionTree dt = new DecisionTree(trainDataB, testDataB);
		dt.printTree();
		dt.printResult();
		System.out.println("Cross validation Accuracy:" + dt.crossValidate(5));
	}

}
