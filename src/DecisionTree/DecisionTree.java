/*
 * TEAM 16 
 * The class of Decision Tree
 * 
 */

package DecisionTree;

import java.util.ArrayList;
import java.util.HashMap;

import DataModel.Data;
import DataModel.DataScanner;
import DataModel.DataSorter;

public class DecisionTree {

	TreeNode root;
	ArrayList<Data> trainData;
	ArrayList<Data> testData;
	HashMap<String, String[]> attrs;
	String[] labelType;
	String[] predictLabels;

	// All the input is the filenames of trainData and testData
	public DecisionTree(String trainData, String testData) {
		DataScanner scanner = new DataScanner();
		this.trainData = scanner.scan(trainData);
		this.testData = scanner.scan(testData);
		labelType = scanner.getLabels(trainData);
		attrs = scanner.getAttributes(trainData);
		root = generateTree(this.trainData, attrs, 0);
	}

	// This constructor is used for validation.
	private DecisionTree(ArrayList<Data> trainData, ArrayList<Data> testData,
			String[] labelType, HashMap<String, String[]> attrs) {
		this.trainData = trainData;
		this.testData = testData;
		this.attrs = attrs;
		this.labelType = labelType;
		root = generateTree(trainData, attrs, 0);
	}

	// This main method to build the tree
	private TreeNode generateTree(ArrayList<Data> dataSet,
			HashMap<String, String[]> attrs, int level) {
		if (dataSet.size() == 0) return null;

		String splitAttr = null;
		double splitVal = 0;
		double maxGainRatio = 0;
		double infoAll = getInfo(dataSet);

		// Prune the tree under certain requirements
		double purity = getPurity(dataSet);
		if (dataSet.size() < 2 || purity > 0.91
				|| (dataSet.size() <= 4 && purity > 0.6)
				|| (dataSet.size() <= 10 && purity > 0.8)) {
			return generateTerminal(dataSet);
		}
		
		for (String attr : attrs.keySet()) {
			double gainRatio = 0;
			double currSplitVal = 0;

			// This is a numeric attribute.
			if (attrs.get(attr) == null) {
				currSplitVal = findSplitPoint(attr, dataSet, infoAll);
				gainRatio = getInfoGainRatioNumeric(attr, dataSet,
						currSplitVal, infoAll);
				// This is discrete attribute
			} else {
				gainRatio = getInfoGainRatioDisc(attr, dataSet, infoAll, attrs);
			}
			if (gainRatio > maxGainRatio) {
				maxGainRatio = gainRatio;
				splitAttr = attr;
				splitVal = currSplitVal;
			}
		}
		boolean isNumeric = (attrs.get(splitAttr) == null);

		TreeNode root;
		HashMap<String, String[]> restAttrs = new HashMap<String, String[]>(
				attrs);
		if (isNumeric) {
			root = new TreeNode(splitAttr, true);
			root.splitVal = splitVal;
			ArrayList<Data> leftSet = splitByValue(dataSet, splitVal,
					splitAttr, false);
			ArrayList<Data> rightSet = splitByValue(dataSet, splitVal,
					splitAttr, true);
			TreeNode left = generateTree(leftSet, restAttrs, level + 1);
			TreeNode right = generateTree(rightSet, restAttrs, level + 1);

			root.left = left;
			root.right = right;

		} else {
			restAttrs.remove(splitAttr);
			root = new TreeNode(splitAttr, false);
			for (String attrType : attrs.get(splitAttr)) {
				ArrayList<Data> childDataSet = splitByType(dataSet, splitAttr,
						attrType);
				TreeNode child = generateTree(childDataSet, restAttrs,
						level + 1);
				root.splitAttrMap.put(attrType, child);
			}
		}
		return root;
	}

	// This method is to generate the leaf.
	private TreeNode generateTerminal(ArrayList<Data> dataSet) {
		String label = null;
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		int max = 0;
		for (Data data : dataSet) {
			if (map.containsKey(data.getLabel())) {
				map.put(data.getLabel(), map.get(data.getLabel()) + 1);
			} else {
				map.put(data.getLabel(), 1);
			}
		}
		for (String s : map.keySet()) {
			if (map.get(s) > max) {
				label = s;
				max = map.get(s);
			}
		}
		TreeNode leaf = new TreeNode(label);
		return leaf;
	}

	/*
	 * This method is to calculate the purity of dataSet, which mean the
	 * percentage of the data with the single same label.
	 */
	private double getPurity(ArrayList<Data> dataSet) {
		int[] labelNum = new int[labelType.length];
		double maxPurity = 0;
		for (Data data : dataSet) {
			for (int i = 0; i < labelNum.length; i++) {
				if (data.getLabel().equals(labelType[i])) {
					labelNum[i]++;
				}
			}
		}
		for (int i = 0; i < labelNum.length; i++) {
			double purity = (double) labelNum[i] / (double) dataSet.size();
			if (purity > maxPurity) maxPurity = purity;
		}
		return maxPurity;
	}

	/*
	 * This method is to find the best point to split a dataSet with a numeric
	 * attribute.
	 */
	private double findSplitPoint(String attr, ArrayList<Data> dataSet,
			double infoAll) {
		// Sort the dataSet according to this attribute.
		DataSorter.dataSort(dataSet, attr);
		double maxGain = 0;
		double splitPoint = 0;
		for (int i = 0; i < dataSet.size() - 1; i++) {

			/*
			 * If the current one and next is in the same class, no need to
			 * split
			 */
			if (dataSet.get(i).getLabel().equals(dataSet.get(i + 1).getLabel())) {
				continue;
			}

			double currSpiltVal = dataSet.get(i).getNumericAttr(attr);
			double infoAttr = getInfoNumeric(dataSet, attr, currSpiltVal);
			double currInfoGain = infoAll - infoAttr;
			if (currInfoGain > maxGain) {
				splitPoint = currSpiltVal;
				maxGain = currInfoGain;
			}
		}
		return splitPoint;

	}

	/*
	 * This method is to calculate the maximum info gain ratio of a dataSet
	 * with the giving numeric attribute.
	 */
	private double getInfoGainRatioNumeric(String attr,
			ArrayList<Data> dataSet, double splitVal, double infoAll) {
		double infoAttr = getInfoNumeric(dataSet, attr, splitVal);
		double infoGain = infoAll - infoAttr;
		double splitInfo = getSplitInfoNumeric(dataSet, splitVal, attr);

		return infoGain / splitInfo;
	}

	// Calculate the Info (Entropy) of the Data Set.
	private double getInfo(ArrayList<Data> dataSet) {
		int[] num = new int[labelType.length];
		for (Data d : dataSet) {
			for (int i = 0; i < labelType.length; i++) {
				if (d.getLabel().equals(labelType[i])) {
					num[i] += 1;
					break;
				}
			}
		}
		double info = 0;
		for (int i = 0; i < num.length; i++) {
			if (num[i] == 0) continue;
			double p = (double) num[i] / (double) dataSet.size();
			info -= (p * log2(p));
		}
		return info;
	}

	// Get the info of numeric attribute
	private double getInfoNumeric(ArrayList<Data> dataSet, String attr,
			double currSplitVal) {
		ArrayList<Data> left = splitByValue(dataSet, currSplitVal, attr, false);
		ArrayList<Data> right = splitByValue(dataSet, currSplitVal, attr, true);
		double leftInfo = getInfo(left);
		double rightInfo = getInfo(right);
		return ((double) left.size() / (double) dataSet.size()) * leftInfo
				+ ((double) right.size() / (double) dataSet.size()) * rightInfo;
	}

	/*
	 * InfoGainRatio = InfoGain / splitInfo. This is the method to get the
	 * gainRatio of numeric attribute.
	 */
	private double getInfoGainRatioDisc(String attr, ArrayList<Data> dataSet,
			double infoAll, HashMap<String, String[]> attrs) {
		double infoAttr = 0;
		for (String AttrJ : attrs.get(attr)) {
			ArrayList<Data> currData = new ArrayList<Data>();
			for (Data data : dataSet) {
				if (data.getDiscreteAttr(attr).equals(AttrJ)) {
					currData.add(data);
				}
			}
			if (currData.size() == 0) continue;
			double infoDj = getInfo(currData);

			infoAttr += (((double) currData.size() / (double) dataSet.size()) * infoDj);
		}
		double infoGain = infoAll - infoAttr;

		double splitInfo = getSplitInfoDiscrete(attr, dataSet, attrs);

		double infoGainRatio = infoGain / splitInfo;

		return infoGainRatio;
	}

	/*
	 * This method is to find the split info of a dataSet with the giving
	 * numeric attribute.
	 */
	private double getSplitInfoNumeric(ArrayList<Data> dataSet,
			double splitVal, String attr) {
		int count = 0;

		for (int i = 0; i < dataSet.size(); i++) {
			if (dataSet.get(i).getNumericAttr(attr) <= splitVal) {
				count++;
			}
		}
		double p1 = (double) count / (double) dataSet.size();
		double p2 = 1 - p1;
		if (p1 == 1 || p2 == 1) return 0.00001;
		return -p1 * log2(p1) - p2 * log2(p2);
	}

	/*
	 * This method is to find the split info of a dataSet with the giving
	 * discrete attribute.
	 */
	private double getSplitInfoDiscrete(String attr, ArrayList<Data> dataSet,
			HashMap<String, String[]> attrs) {

		double splitInfo = 0;
		for (String AttrJ : attrs.get(attr)) {
			int attrNum = 0;
			for (Data data : dataSet) {
				if (data.getDiscreteAttr(attr).equals(AttrJ)) {
					attrNum++;
				}
			}
			if (attrNum == 0) continue;

			double p = (double) attrNum / (double) dataSet.size();
			if (p == 1.0) return 0.00001;
			splitInfo -= (p * log2(p));
		}
		return splitInfo;
	}

	// Select the data with chosen attribute.
	private ArrayList<Data> splitByType(ArrayList<Data> dataSet,
			String splitAttr, String attrType) {
		ArrayList<Data> res = new ArrayList<Data>();
		for (Data d : dataSet) {
			if (d.getDiscreteAttr(splitAttr).equals(attrType)) {
				res.add(d);
			}
		}
		return res;
	}

	// Select data according to a numeric attribute
	private ArrayList<Data> splitByValue(ArrayList<Data> dataSet,
			double splitVal, String attr, boolean right) {
		ArrayList<Data> res = new ArrayList<Data>();
		for (Data d : dataSet) {
			if (right) {
				if (d.getNumericAttr(attr) > splitVal) {
					res.add(d);
				}
			} else {
				if (d.getNumericAttr(attr) <= splitVal) {
					res.add(d);
				}
			}
		}
		return res;
	}

	
	//calculate log2(n)
	private double log2(double a) {
		return Math.log(a) / Math.log(2);
	}

	// Get the predicted label of a data based on the Decision Tree
	private String classify(Data data) {
		TreeNode cur = root;
		while (cur != null && cur.label == null) {
			if (cur.isNumeric) {
				if (data.getNumericAttr(cur.attribute) <= cur.splitVal) {
					cur = cur.left;
				} else {
					cur = cur.right;
				}
			} else {
				cur = cur.splitAttrMap.get(data.getDiscreteAttr(cur.attribute));
			}
		}
		if (cur == null) return "null";
		return cur.label;
	}

	// Return the accuracy of predication if the actual labels are provided
	private double getAccuracy(String[] predictLabels, String[] realLabels) {
		if (predictLabels.length != realLabels.length) return -1;
		double correct = 0;
		double wrong = 0;
		for (int i = 0; i < predictLabels.length; i++) {
			if (predictLabels[i].equals(realLabels[i])) {
				correct++;
			} else
				wrong++;
		}
		double acur = correct / (correct + wrong);
		return acur;
	}

	// Shuffle the data set.
	private void shuffle(ArrayList<Data> dataSet) {
		for (int i = 0; i < dataSet.size(); i++) {
			int k = rand(0, i);
			Data temp = dataSet.get(k);
			dataSet.set(k, dataSet.get(i));
			dataSet.set(i, temp);
		}
	}

	// Return a random integer between lower(inclusive) and higher(exclusive);
	private int rand(int lower, int higher) {
		return lower + (int) ((higher - lower + 1) * Math.random());

	}

	// Print the decision tree
	private void print(TreeNode curr, int level) {

		// Has reached the leaf.
		if (curr.label != null) {
			System.out.println("Label:" + curr.label);
			return;
		}

		System.out.println(curr.attribute);
		if (curr.left != null) {
			for (int i = 0; i < level + 1; i++) {
				System.out.print("     ");
			}
			System.out.print(" <= " + curr.splitVal + " : ");
			print(curr.left, level + 1);
		}
		if (curr.right != null) {
			for (int i = 0; i < level + 1; i++) {
				System.out.print("     ");
			}
			System.out.print(" >  " + curr.splitVal + " : ");
			print(curr.right, level + 1);
		}
		if (curr.splitAttrMap != null) {
			for (String key : curr.splitAttrMap.keySet()) {
				if (curr.splitAttrMap.get(key) != null) {
					for (int i = 0; i < level + 1; i++) {
						System.out.print("     ");
					}
					System.out.print(key + " : ");
					print(curr.splitAttrMap.get(key), level + 1);
				}
			}
		}
	}

	/*
	 * This method will return the string array of the predicted labels of all
	 * the test data.
	 */
	public String[] classify() {
		String[] labels = new String[this.testData.size()];
		for (int i = 0; i < this.testData.size(); i++) {
			labels[i] = classify(this.testData.get(i));
		}
		return labels;
	}

	 /* This method will run the cross validate with shuffled train data
	  * for 10 times and get the average accuracy.
	  */  
	public double crossValidate(int k) { 
		double sum = 0;
		for (int i = 0 ; i < 20; i++) {
			sum += validate(k);
		}
		return sum / 20;
	}
	
	/*
	 * This method is validate the decision tree model by k - folds validation.
	 */
	private double validate(int k) {
		ArrayList<Data> allData = trainData;
		shuffle(allData);
		ArrayList<Double> accurs = new ArrayList<Double>();
		if (k < 1 || k > allData.size()) return -1;
		int foldLength = allData.size() / k;
		for (int i = 0; i < k; i++) {
			ArrayList<Data> fold = new ArrayList<Data>();
			ArrayList<Data> rest = new ArrayList<Data>();
			fold.addAll(allData.subList(i * foldLength, (i + 1) * foldLength));
			rest.addAll(allData.subList(0, i * foldLength));
			rest.addAll(allData.subList((i + 1) * foldLength, allData.size()));

			String[] predict = new String[fold.size()];

			DecisionTree validateTree = new DecisionTree(rest, fold,
					this.labelType, this.attrs);
			for (int j = 0; j < fold.size(); j++) {
				predict[j] = validateTree.classify(fold.get(j));
			}

			String[] real = getLabels(fold);
			accurs.add(getAccuracy(predict, real));
		}
		double accur = 0;
		for (int i = 0; i < accurs.size(); i++) {
			accur += accurs.get(i);
		}
		accur /= accurs.size();
		return accur;
	}

	// Return the array of labels from a ArrayList of Customer
	public String[] getLabels(ArrayList<Data> data) {
		String[] labels = new String[data.size()];
		for (int i = 0; i < data.size(); i++)
			labels[i] = data.get(i).getLabel();
		return labels;
	}

	// Print the decision tree
	public void printTree() {
		TreeNode curr = this.root;
		print(curr, 0);
	}

	// print the predicted result of test data
	public void printResult() {
		String[] predictedLabels = classify();
		for (int i = 0; i < predictedLabels.length; i++) {
			System.out.println(testData.get(i) + " Predicted:"
					+ predictedLabels[i]);
		}
	}

}
