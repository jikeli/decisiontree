Team 16

Members:

Jike Li
Avanthi Reddy
Meiqui Li
Liquin Chen
Xintong Huang

Description:

This is the Decision Tree Implementation Algorithm based on C4.5. This program will generate a decision tree according to the input training data. Also, it will classify the input test data. The input data should be in ".arff" format.

User Guide: 
To run the program, simply run the main.java in the Main package. See detailS in the comment.

Files end with "Selection" are for Part A
Files end with "Intro" are for Part B

Reference:
http://en.wikipedia.org/wiki/C4.5_algorithm